package com.nowolfx;

/**
 * @author Zhn17
 * <-> 2018-09-13 <->
 * com.nowolfx
 **/
public class Main {

    /*
     * Client name
     */

    public final String CLIENT_NAME = "Test";

    /*
     * Check if "CLIENT_NAME" contains "WolfX", "Wolfx", "W0lfx" etc
     * Put it in your client's start void
     */

    public void onInit() {
        if (CLIENT_NAME.contains("Wolfx") | CLIENT_NAME.contains("W0lfx") | CLIENT_NAME.contains("WolfX"))
            System.exit(0);
    }
}